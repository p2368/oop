<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
$frog = new Frog("buduk");
$ape = new Ape("kera sakti");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>

<body>
    <h3><strong>output akhir</strong></h3>
    <ul style="list-style-type: none; padding-left: 10px;">
        <li>Name : <?= $sheep->get_name(); ?></li>
        <li>Legs : <?= $sheep->get_legs(); ?></li>
        <li>Cold Blooded : <?= $sheep->get_cold_blooded(); ?></li>
    </ul>
    <ul style="list-style-type: none; padding-left: 10px;">
        <li>Name : <?= $frog->get_name(); ?></li>
        <li>Legs : <?= $frog->get_legs(); ?></li>
        <li>Cold Blooded : <?= $frog->get_cold_blooded(); ?></li>
        <li>Jump : <?= $frog->jump(); ?></li>
    </ul>
    <ul style="list-style-type: none; padding-left: 10px;">
        <li>Name : <?= $ape->get_name(); ?></li>
        <li>Legs : <?= $ape->get_legs(); ?></li>
        <li>Cold Blooded : <?= $ape->get_cold_blooded(); ?></li>
        <li>Yell : <?= $ape->yell(); ?></li>
    </ul>

</body>

</html>